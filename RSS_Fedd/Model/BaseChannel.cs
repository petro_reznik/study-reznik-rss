﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSS_Feed.Model
{
    class BaseChannel:IChannel
    {
        public string Title { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Link { get; set; } = string.Empty;
        public string Copyright { get; set; } = string.Empty;
        // question
        public List<IItem> Items { get; set; } = new List<IItem>();
    }
}
