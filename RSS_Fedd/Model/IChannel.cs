﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSS_Feed.Model
{
    public interface IChannel
    {
        string Title { get; set; }
        string Link { get; set; }
        string Description { get; set; }
        string Copyright { get; set; }
        List<IItem> Items { get; set; }        

    }
}
