﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSS_Feed
{
    public interface IItem
    {
        string Title { get; set; }
        string Link { get; set; }
        string Description { get; set; }
        string Category { get; set; }
        string Comments { get; set; }
        DateTime Date { get; set; }
    }
}
