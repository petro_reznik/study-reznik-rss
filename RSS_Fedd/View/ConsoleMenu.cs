﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSS_Feed.DAL;
using RSS_Feed.Model;

namespace RSS_Feed.View
{
    public static class ConsoleMenu
    {

        public static void Start(IRssReader reader, IChannel channel) {

            printMenu();
            string input = Console.ReadLine();
            bool menuCondition = true;
            while (menuCondition)
            {
                switch (input)
                {
                    case "1":
                        AddFeed(reader, channel);
                        break;
                    case "2":
                        Refresh(reader, channel);
                        break;
                    case "3":
                        Console.Clear();
                        break;
                    case "4":
                        menuCondition = false;
                        break;

                    default:
                        Console.WriteLine("Invalid Input");
                        break;
                }
                Console.Clear();
                printMenu();
                Console.Write("Please make a choice: ");
                input = Console.ReadLine();
            }
        }

        static void getChannel(string path, IRssReader reader, IChannel chan) {
            if (reader.IsValidFeed(path, chan))
            {
                chan.PlotRSSToConsole();
            } else
            {
                Console.WriteLine("RSS Feed is not valid. Try again or " + "Cancel");
            }
        }

        static void AddFeed(IRssReader reader, IChannel chan) {
            bool condition = true;
            Console.Write("Enter feed or Cancel: ");
            string path = Console.ReadLine();
            while (condition)
            {
                if (path == "Cancel")
                {
                    condition = false;
                    break;
                }
                getChannel(path, reader, chan);
                Console.Write("Enter feed or Cancel: ");
                path = Console.ReadLine();
            }
        }

        static void Refresh(IRssReader reader, IChannel chan) {
            bool condition = true;
            Console.Write("Enter feed or Cancel: ");
            string path = Console.ReadLine();
            while (condition)
            {
                if (path == "Cancel")
                {
                    condition = false;
                    break;
                }
                reader.RefreshCurrent(path, chan);
                chan.PlotRSSToConsole();
                Console.Write("Enter feed or Cancel: ");
                path = Console.ReadLine();
            }
        }

        static void printMenu() {
            Console.WriteLine("1. Add RSS Feed");
            Console.WriteLine("2. Refresh");
            Console.WriteLine("3. Clear Console");
            Console.WriteLine("4. Exit");
        }

    }
}
