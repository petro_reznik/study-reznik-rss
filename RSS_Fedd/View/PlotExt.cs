﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSS_Feed.Model;

namespace RSS_Feed.View
{
    public static class PlotExt
    {
        public static void PlotRSSToConsole(this IChannel channel) {
            PrintInColorConsole(channel.Title, ConsoleColor.Red);
            PrintInColorConsole(channel.Description, ConsoleColor.DarkRed);
            Console.WriteLine(channel.Link);
            Console.WriteLine();
            Console.WriteLine(new string('-', 20));
            foreach (var item in channel.Items)
            {
                PrintInColorConsole(item.Title, ConsoleColor.Magenta);
                PrintInColorConsole(item.Description, ConsoleColor.Cyan);
                PrintInColorConsole(item.Link, ConsoleColor.DarkYellow);
                PrintInColorConsole(item.Date.ToString(), ConsoleColor.Green);
                Console.WriteLine(new string('-', 20));
            }
        }

        static void PrintInColorConsole(string text, ConsoleColor color) {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ResetColor();
            Console.WriteLine();
        }
    }
}
