﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSS_Feed.Model;
using System.Xml;
using System.ServiceModel.Syndication;

namespace RSS_Feed.DAL
{
    class RssReaderSyndication : IRssReader
    {
        public List<IItem> getItems(string path) {            
            List<IItem> items = new List<IItem>();

            try
            {
                XmlReader feedReader = XmlReader.Create(path);
                SyndicationFeed Channel = SyndicationFeed.Load(feedReader);
                feedReader.Close();
                IItem tempItem = new BaseItem();
                foreach (SyndicationItem RSI in Channel.Items)
                {
                    tempItem.Title = RSI.Title != null ? RSI.Title.Text : "";
                    tempItem.Link = RSI.Links != null ?
                        string.Join("; ", RSI.Links.Select(x => x.Uri.AbsoluteUri)) : "";
                    tempItem.Description = RSI.Summary != null ? RSI.Summary.Text : "";
                    tempItem.Category = RSI.Categories != null ?
                        string.Join("; ", RSI.Categories.Select(x => x.Name)) : "";
                    tempItem.Date = RSI.PublishDate.DateTime;
                    items.Add(tempItem);
                }
            }
            catch (System.ArgumentException)
            {
                Console.WriteLine("Argument exception");                
            }
            catch (System.IO.FileNotFoundException)
            {
                Console.WriteLine("File NOT found!");                
            }
            return items;
        }
        
        public bool IsValidFeed(string path, IChannel channel) {
            try
            {                
                List<IItem> items = new List<IItem>();
                StringBuilder rssContent = new StringBuilder();

                XmlReader feedReader = XmlReader.Create(path);
                SyndicationFeed Channel = SyndicationFeed.Load(feedReader);
                feedReader.Close();

                channel.Title = Channel.Title.Text;
                channel.Description = Channel.Description.Text;
                channel.Copyright = Channel.Copyright!=null? Channel.Copyright.Text:"";
                channel.Link = Channel.Links != null ?
                        string.Join("; ", Channel.Links.Select(x => x.Uri.AbsoluteUri)) : "";
                channel.Items = getItems(path);
                return true;
            }
            catch (System.ArgumentException)
            {
                Console.WriteLine("Argument exception");
                return false;
            }
            catch (System.IO.FileNotFoundException)
            {
                Console.WriteLine("File NOT found!");
                return false;
            }
        }

        public void RefreshCurrent(string path, IChannel channel) {
            List<IItem> tempItems = getItems(path);
            if (channel.Items.Count != 0 && tempItems.Count != 0)
            {
                IItem lastItem = channel.Items.OrderByDescending(x => x.Date).FirstOrDefault();
                channel.Items.AddRange(tempItems.Where(x => x.Date > lastItem.Date));

            } else
            {
                channel.Items = tempItems;
            }
        }
    }
}
