﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSS_Feed.Model;
using System.Xml;

namespace RSS_Feed.DAL
{
    class RssReaderXml : IRssReader
    {
        public bool IsValidFeed(string path, IChannel channel) {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);

                XmlNodeList nodeList;
                XmlNode root = doc.DocumentElement;
                nodeList = root.ChildNodes;

                foreach (XmlNode node in nodeList)
                {
                    foreach (XmlNode chanItem in node)
                    {
                        if (chanItem.Name == "title")
                        {
                            channel.Title = chanItem.InnerText;
                        }
                        if (chanItem.Name == "description")
                        {
                            channel.Description = chanItem.InnerText;
                        }
                        if (chanItem.Name == "copyright")
                        {
                            channel.Copyright = chanItem.InnerText;
                        }
                        if (chanItem.Name == "link")
                        {
                            channel.Link = chanItem.InnerText;
                        }
                    }
                    channel.Items = getItems(path);
                }
                return true;
            }
            catch (System.IO.FileNotFoundException)
            {
                Console.WriteLine("File NOT found!");
                return false;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
            finally {

            }
        }

        public List<IItem> getItems(string path) {
            try
            {
                List<IItem> items = new List<IItem>();
                XmlDocument doc = new XmlDocument();
                doc.Load(path);

                XmlNodeList nodeList;
                XmlNode root = doc.DocumentElement;
                nodeList = root.ChildNodes;

                foreach (XmlNode node in nodeList)
                {
                    foreach (XmlNode chanItem in node)
                    {
                        if (chanItem.Name == "item")
                        {
                            XmlNodeList itemsList = chanItem.ChildNodes;
                            IItem tempItem = new BaseItem();

                            foreach (XmlNode item in itemsList)
                            {
                                if (item.Name == "title")
                                {
                                    tempItem.Title = item.InnerText;
                                }
                                if (item.Name == "link")
                                {
                                    tempItem.Link = item.InnerText;
                                }
                                if (item.Name == "description")
                                {
                                    tempItem.Description = item.InnerText;
                                }
                                if (item.Name == "category")
                                {
                                    tempItem.Category = item.InnerText;
                                }
                                if (item.Name == "comments")
                                {
                                    tempItem.Comments = item.InnerText;
                                }
                                if (item.Name == "pubDate")
                                {
                                    tempItem.Date = Convert.ToDateTime(item.InnerText);
                                }
                            }
                            items.Add(tempItem);
                        }
                    }
                }

                return items;
            }
            catch (System.IO.FileNotFoundException)
            {
                Console.WriteLine("File NOT found!");
                return new List<IItem>();
            }
            catch (Exception)
            {
                return new List<IItem>();
            }
        }

        public void RefreshCurrent(string path, IChannel channel) {


            List<IItem> tempItems = getItems(path);
            if (channel.Items.Count != 0 && tempItems.Count != 0)
            {
                IItem lastItem = channel.Items.OrderByDescending(x => x.Date).FirstOrDefault();
                channel.Items.AddRange(tempItems.Where(x => x.Date > lastItem.Date));

            } else
            {
                channel.Items = tempItems;
            }

        }
    }
}
