﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSS_Feed.Model;

namespace RSS_Feed.DAL
{
    public interface IRssReader
    {
        bool IsValidFeed(string path, IChannel channel);

        List<IItem> getItems(string path);

        void RefreshCurrent(string path, IChannel channel);
    }
}
